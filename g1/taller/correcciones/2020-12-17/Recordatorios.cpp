#include <iostream>
#include <list>

using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
            // ene, feb, mar, abr, may, jun
            31, 28, 31, 30, 31, 30,
            // jul, ago, sep, oct, nov, dic
            31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
public:
    // Completar declaraciones funciones
    Fecha(int mes, int dia);
    int mes();
    int dia();

    void incrementar_dia();

#if EJ >= 9 // Para ejercicio 9
    bool operator==(Fecha o);
    bool operator<(Fecha o);
#endif

private:
    //Completar miembros internos
    int dia_;
    int mes_;
};

Fecha::Fecha(int mes, int dia) : mes_(mes), dia_(dia)  {}

int Fecha::mes() {
    return mes_;
}

int Fecha::dia() {
    return dia_;
}

ostream& operator<<(ostream& os, Fecha d) {
    os << d.dia()<< "/" << d.mes();
    return os;
}

#if EJ >= 9
bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia() == o.dia();
    bool igual_mes = this->mes() == o.mes();
    return igual_dia && igual_mes;
}

bool Fecha::operator<(Fecha o) {
    bool igual_dia = this->mes() < o.mes();
    bool igual_mes = this->dia() < o.dia();
    return igual_dia && igual_mes;
}
#endif

void Fecha::incrementar_dia(){
    if(dias_en_mes(mes()) > dia())
    {
        dia_ += 1;
    }
    else{
        mes_ += 1;
        dia_ = 1;
    }
}

// Ejercicio 11, 12

// Clase Horario
class Horario {
public:
    // Completar declaraciones funciones
    Horario(uint hora, uint min);
    uint hora();
    uint min();

    bool operator<(Horario h);
    bool operator==(Horario h);
private:
    //Completar miembros internos
    uint hora_;
    uint min_;
};


bool Horario::operator==(Horario h) {
    bool igual_hora = this->hora() == h.hora_;
    bool igual_min = this->min() == h.min();
    return  igual_hora && igual_min;
}

bool Horario::operator<(Horario h) {
    bool es_menor;
    if(this->hora() == h.hora()){
        if (this->min() == h.min())
        {
            es_menor = false;
        }
        else
        {
            if(this->min() < h.min())
            {
                es_menor = true;
            }
            else
            {
                es_menor = false;
            }
        }
    } else if(this->hora() > h.hora())
    {
        es_menor = false;
    } else{
        es_menor = true;
    }

    return es_menor;
}
Horario::Horario(uint hora, uint min) : hora_(hora), min_(min){}

uint Horario::hora() {
    return hora_;
}

uint Horario::min() {
    return min_;
}

ostream& operator<<(ostream& os, Horario h) {
    os << h.hora()<< ":" << h.min();
    return os;
}
// Ejercicio 13

// Clase Recordatorio
class Recordatorio {
public:
    // Completar declaraciones funciones
    Recordatorio(Fecha fecha, Horario horario, string mensaje);
    string Recordatorio_Mensaje();
    Fecha Recordatorio_Fecha();
    Horario Recordatorio_Horario();
    bool operator==(Recordatorio r);
    bool operator<(Recordatorio r);
private:
    //Completar miembros internos
    string mensaje_;
    Fecha fecha_;
    Horario horario_;

};
Recordatorio::Recordatorio(Fecha fecha, Horario horario, string mensaje) : fecha_(fecha), horario_(horario), mensaje_(mensaje) {}

string Recordatorio::Recordatorio_Mensaje(){
    return mensaje_;
}

Fecha Recordatorio::Recordatorio_Fecha() {
    return fecha_;
}

Horario Recordatorio::Recordatorio_Horario() {
    return horario_;
}

ostream& operator<<(ostream& os, Recordatorio r) {
    os << r.Recordatorio_Mensaje() << " @ " << r.Recordatorio_Fecha() << " " << r.Recordatorio_Horario();
    return os;
}

bool Recordatorio::operator==(Recordatorio r) {
    bool igual_mensaje = this->Recordatorio_Mensaje() == r.mensaje_;
    bool igual_horario = this->Recordatorio_Horario() == r.horario_;
    bool igual_fecha = this->Recordatorio_Fecha()   == r.fecha_;
    return igual_mensaje && igual_horario && igual_fecha;
}

bool Recordatorio::operator<(Recordatorio r) {
    bool menor_horario = this->Recordatorio_Horario() < r.horario_;
    bool menor_fecha = this->Recordatorio_Fecha()   < r.fecha_;

    return  menor_horario && menor_fecha || menor_horario && menor_fecha==false;
}

// Ejercicio 14

// Clase Agenda

class Agenda {
public:
    Agenda(Fecha fecha_inicial);
    void agregar_recordatorio(Recordatorio rec);
    void incrementar_dia();
    vector<Recordatorio> recordatorio__de_hoy();
    Fecha hoy();

private:
    Fecha fecha_;
    vector<Recordatorio> recordatorio_;

};

Agenda::Agenda(Fecha fecha_inicial) : fecha_(fecha_inicial), recordatorio_(){}

vector<Recordatorio> Agenda::recordatorio__de_hoy() {
    vector<Recordatorio> vector_recordatoriosHoy;
    for (int i = 0; i < recordatorio_.size(); ++i) {
        if (recordatorio_[i].Recordatorio_Fecha() == hoy()){
            vector_recordatoriosHoy.push_back(recordatorio_[i]);
        }
    }

    sort(vector_recordatoriosHoy.begin(), vector_recordatoriosHoy.end());

    return vector_recordatoriosHoy;
}

Fecha Agenda::hoy() {
    return fecha_;
}

void Agenda::incrementar_dia() {
    fecha_.incrementar_dia();
}

void Agenda::agregar_recordatorio(Recordatorio rec) {
    recordatorio_.push_back(rec);
}

ostream& operator<<(ostream& os, Agenda a){

    os << a.hoy() << endl;
    os << "=====" << endl;
    for (int i = 0; i < a.recordatorio__de_hoy().size() ; ++i) {

        os << a.recordatorio__de_hoy()[i] << endl;

    }
    return os;
}







