

#include "Conjunto.h"

template <class T>
const T& Conjunto<T>::auxiliarMinimo(Nodo* n) {
    Nodo* helpNode = n;
    while (true){
        if(helpNode->izq == nullptr){
            break;
        }else{
            helpNode = helpNode->izq;
        }
    }
    return helpNode->valor;
}

template <class T>
Conjunto<T>::Conjunto() : largo(0), first(nullptr) {
    // Completar
}

template <class T>
Conjunto<T>::~Conjunto() {
    eliminar(this->first);
}


template <class T>
bool Conjunto<T>::pertenece(const T& key) const {
    Nodo* root = this->first;

    while (root != nullptr){

        if ( root->valor == key){
            return true;
        }

        if (root->valor < key){
            root = root->der;
        } else{
            root = root->izq;
        }

    }

    return false;
}

template <class T>
void Conjunto<T>::insertar(const T& key) {
    Nodo* root = this->first;

    if (largo == 0){
        first = new Nodo(key);

        largo++;
    }else if (!pertenece(key)){
        while( root != nullptr){
            if (root->valor < key && root->der == nullptr){
                root->der = new Nodo(key);
                root = nullptr;
                largo++;

            } else if(root->valor > key && root->izq == nullptr){
                root->izq = new Nodo(key);
                root = nullptr;
                largo++;
            }else if(root->valor < key){
                root = root->der;
            }else{
                root = root->izq;
            }
        }
    }
}

template <class T>
void Conjunto<T>::remover(const T& key) {        // VER CASO EN QUE SE SACA LA RAIZ Y QUEDAN DOS ARBOLES VOLANDO

    Nodo* root = this->first;
    Nodo* helpNode = nullptr;
    if (pertenece(key)){
        while( root != nullptr){
            if (root->valor < key){
                helpNode = root;
                root = root->der;
            } else if(root->valor > key){
                helpNode = root;
                root = root->izq;
            }else if(root->valor == key && root->izq == nullptr && root->der == nullptr){
                if(root == first){
                    first = nullptr;
                }else if(helpNode->der == root){
                    helpNode->der = nullptr;
                }else{
                    helpNode->izq = nullptr;
                }
                delete root;
                largo--;
                break;
            }else if(root->valor == key && root->izq != nullptr && root->der != nullptr){
                const T a = auxiliarMinimo(root->der);
                remover(a);
                if(root == first){
                    first->valor = a;
                }
                root->valor = a;
                break;
            }else if(root->valor == key && root->izq == nullptr && root->der != nullptr){
                if(root == first){
                    first = root->der;
                }else if(helpNode->izq == root){
                    helpNode->izq = root->der;
                }else{
                    helpNode->der = root->der;
                }

                delete root;
                largo--;
                break;
            }else if(root->valor == key && root->izq != nullptr && root->der == nullptr){
                if(root == first){
                    first = root->izq;
                }else if(helpNode->der == root){
                    helpNode->der = root->izq;
                }else{
                    helpNode->izq = root->izq;
                }

                delete root;
                largo--;
                break;
            }
        }
    }


}

template <class T>
const T& Conjunto<T>::siguiente(const T& key) {
    Nodo* root = this->first;
    Nodo* temp;
    while(root != nullptr){
        if(root->valor == key && root->der != nullptr){
            return auxiliarMinimo(root->der);
        }else if(root->der == nullptr && root->valor == key){
            if(isLeftSon(key)){
                return temp->valor;
            }else{
                return closerLeftNode(key);
            }
        }else if(root->valor < key){
            temp = root;
            root = root->der;
        }else if(root->valor > key){
            temp = root;
            root = root->izq;
        }
    }

}

template <class T>
const T& Conjunto<T>::minimo() const {
    Nodo* root = this->first;
    while(root != nullptr){
        if(root->izq != nullptr){
            root = root->izq;
        }else{
            return root->valor;
        }
    }
}



template <class T>
const T& Conjunto<T>::maximo() const {
    Nodo* root = this->first;
    while (root != nullptr){
        if (root->der == nullptr){
            break;
        } else{
            root = root->der;
        }
    }
    return root->valor;
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {

    return largo;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}

template<class T>
const T &Conjunto<T>::closerLeftNode(const T &elem) {
    Nodo* helpNode = this->first;
    Nodo* ultimoNode;
    while(helpNode != nullptr){
        if(helpNode->valor < elem){
            if(helpNode->izq != nullptr) {
                ultimoNode = helpNode->izq;
            }
            helpNode = helpNode->der;
        }else if(helpNode->valor > elem){
            if(helpNode->izq != nullptr) {
                ultimoNode = helpNode->izq;
            }
            helpNode = helpNode->izq;
        }else{
            break;
        }

    }
    return ultimoNode->valor;
}

template<class T>
bool Conjunto<T>::isLeftSon(const T &key) {
    Nodo* root = this->first;
    while(root != nullptr){
        if(root->izq != nullptr){
            if(root->izq->valor == key){
                return true;
            }
        }else if(root->valor < key){
            root = root->der;
        }else if(root->valor > key){
            root = root->izq;
        }
    }
    return false;
}

template<class T>
void Conjunto<T>::eliminar(Conjunto::Nodo *node) {
    if(node == nullptr){
        return;
    } else{
        eliminar(node->izq);
        eliminar(node->der);
        delete node;
    }
}









