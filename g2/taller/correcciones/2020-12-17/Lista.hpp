#include "Lista.h"

Lista::Lista() : largo_(0), primero_(nullptr), ultimo_(nullptr) {

}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {
   int i=0;
   while(i<longitud())
        eliminar(i);

}

Lista& Lista::operator=(const Lista& aCopiar) {
    // Completar

    this->~Lista();

    Nodo* nodo = aCopiar.primero_;

    for (int j = aCopiar.longitud()-1; j >= 0 ; --j) {

        this->agregarAtras(nodo->info);
        nodo = nodo->siguiente;
    }

    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    Nodo* nodo1 = this->primero_;
    Nodo* nodo2 = new Nodo(elem);
    if (nodo1 != nullptr) {
        nodo1->anterior = nodo2;
        nodo2->siguiente = nodo1;
        nodo2->anterior = nullptr;
        primero_ = nodo2;
        largo_++;
    } else{
        primero_ = nodo2;
        ultimo_ = nodo2;
        largo_++;
    }
}

void Lista::agregarAtras(const int& elem) {
    Nodo* nodo1 = this->ultimo_;
    Nodo* nodo2 = new Nodo(elem);
    if (nodo1 != nullptr) {
        nodo1->siguiente = nodo2;
        nodo2->anterior = nodo1;
        nodo2->siguiente = nullptr;
        ultimo_ = nodo2;
        largo_++;
    } else{
        ultimo_ = nodo2;
        primero_ = nodo2;
        largo_++;
    }
}

void Lista::eliminar(Nat i) {
    Nodo* nodo1 = this->primero_;
    int indice = 0;
    while(nodo1 != nullptr && indice <= i) {
        if (indice == i ){
            if (nodo1->anterior != nullptr && nodo1->siguiente != nullptr){
                (nodo1->siguiente)->anterior = nodo1->anterior;
                (nodo1->anterior)->siguiente = nodo1->siguiente;
                delete nodo1;
                largo_--;
                break;
            }else if( nodo1->siguiente == nullptr && nodo1->anterior != nullptr){
                (nodo1->anterior)->siguiente = nullptr;
                this->ultimo_ = nodo1->anterior;
                delete nodo1;
                largo_--;
                break;
            } else if( nodo1->siguiente != nullptr && nodo1->anterior == nullptr){
                (nodo1->siguiente)->anterior = nullptr;
                this->primero_ = nodo1->siguiente;
                delete nodo1;
                largo_--;
                break;
            }else{
                delete nodo1;
                this->primero_ = nullptr;
                this->ultimo_ = nullptr;
                largo_--;
                break;
            }
        }else{
            nodo1 = nodo1->siguiente;
            indice++;
        }

    }
}
int Lista::longitud() const {

    return largo_;
}

const int& Lista::iesimo(Nat i) const {
    int n = 0;
    Nodo* nodo1 = this->primero_;
    while (n <= longitud()-1){
        if(n == i){
            return nodo1->info;
        }else{
            nodo1 = nodo1->siguiente;
            n++;
        }
    }
    return nodo1->info;
}

int& Lista::iesimo(Nat i) {
    int n = 0;
    Nodo* nodoAux = this->primero_;
    while (n <= longitud()-1){
        if(n == i){
            return nodoAux->info;
        }else{
            nodoAux = nodoAux->siguiente;
            n++;
        }
    }
    return nodoAux->info;
}

void Lista::mostrar(ostream& o) {
    // Completar
}

