template <typename T>
string_map<T>::string_map(): primero(nullptr), tamanio(0){
}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() {
    *this = aCopiar;
} // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d) {
    vector<string> str;
    for(int i = 0; i < this->keys.size(); i++){
        erase(this->keys[i]);
    }
    this->keys = str;
    for(int i = 0; i < d.keys.size(); i++){
        T significado = d.at(d.keys[i]);
        this->insert(make_pair(d.keys[i],significado));
    }
    return *this;
}

template <typename T>
string_map<T>::~string_map(){
    eraseFrom(this->primero);
}

template<typename T>
void string_map<T>::insert(const pair<string, T>& aDefinir) {
    if(this->primero == nullptr){
        this->primero = new Nodo();
    }
    string str = aDefinir.first;
    T key = aDefinir.second;
    Nodo *aux = this->primero;
    for(int i = 0; i <= str.size(); i++){
        if(i < str.size()){
            int pos = int(str[i]);
            if(aux->proximos[pos] == nullptr){
                aux->proximos[pos] = new Nodo();
                aux = aux->proximos[pos];
            }else{
                aux = aux->proximos[pos];
            }
        }else{
            if(aux->defin == nullptr){
                aux->defin = new T;
                *aux->defin = key;
                this->keys.push_back(str);
            }else{
                *aux->defin = key;
            }
        }
    }
    tamanio++;
}


template <typename T>
T& string_map<T>::operator[](const string& key){
    // COMPLETAR
}


template <typename T>
int string_map<T>::count(const string& key) const{
    int def = 0;
    if(!this->empty()){
        Nodo* aux = this->primero;
        int i = 0;
        while(i < key.size() && aux != nullptr){
            int pos = int(key[i]);
            if(aux->proximos[pos] != nullptr){
                aux = aux->proximos[pos];
            }else{
                def = 0;
                break;
            }
            i++;
        }
        if(aux->defin != nullptr){
            def = 1;
        }
    }

    return def;
}


template <typename T>
const T& string_map<T>::at(const string& key) const {
    Nodo* aux = this->primero;
    int i = 0;
    while(i < key.size()){
        int pos = int(key[i]);
        aux = aux->proximos[pos];
        i++;
    }
    return *aux->defin;
}

template <typename T>
T& string_map<T>::at(const string& key) {
    Nodo* aux = this->primero;
    int i = 0;
    while(i < key.size()){
        int pos = int(key[i]);
        aux = aux->proximos[pos];
        i++;
    }
    return *aux->defin;
}

template <typename T>
void string_map<T>::erase(const string& key) {
    Nodo* ActualNode = this->primero;
    Nodo* NodeLast = this->primero;
    int l = 0;
    int i = 0;
    while(i < key.size()){
        int pos = int(key[i]);
        if (ActualNode->defin != nullptr || children(ActualNode) > 1) {
            NodeLast = ActualNode;
            l = i;
        }
        ActualNode = ActualNode->proximos[pos];
        i++;
    }
    delete(ActualNode->defin);
    ActualNode->defin = nullptr;

    if(children(ActualNode) == 0){
        int pos = int(key[l]);
        Nodo* siguiente = NodeLast->proximos[pos];
        NodeLast->proximos[pos] = nullptr;
        l++;
        NodeLast = siguiente;
        erase(NodeLast,l,key);
    }
    tamanio--;
}

template <typename T>
int string_map<T>::size() const{
    return tamanio;
}

template <typename T>
bool string_map<T>::empty() const{
    return this->primero == nullptr && this->size() == 0;
}

template<typename T>
void string_map<T>::eraseFrom(string_map::Nodo *node) {
    if(node != nullptr){
        for(int i = 0; i < node->proximos.size(); i++){
            eraseFrom(node->proximos[i]);
        }
        delete node->defin;
        node->defin = nullptr;
        delete node;
    }
}

template<typename T>
int string_map<T>::children(string_map::Nodo *node) {
    int ans = 0;
    for(int i = 0; i < node->proximos.size(); i++){
        if(node->proximos[i] != nullptr){
            ans = ans + 1;
        }
    }
    return ans;
}

template<typename T>
void string_map<T>::erase(string_map::Nodo *&n, int l, const string &key) {
    if(n != nullptr){
        int pos = int(key[l]);
        Nodo* siguiente = n->proximos[pos];
        delete n;
        n = siguiente;
        erase(n,l+1,key);
    }
}



